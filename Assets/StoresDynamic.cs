﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;


public class StoresDynamic : MonoBehaviour {

	public RawImage storeImageName;
	public Text storeName;
	public Text city;
	public Text address;
	public Text pinCode;
	public Text email;
	public Text phno;

	// Use this for initialization
	IEnumerator Start () 
	{

		string url = "http://www.whitethoughtsdigital.com/dev/armall/getShopDetails.php";
		WWWForm form1 = new WWWForm();
		form1.AddField ("sid",1);
		//		form1.AddField ("flag",1);
		WWW www = new WWW (url,form1);
		//string checkCouponmsg = www1.text;
		yield return www;
		string jsonData = "";
		if (www.error == null)
		{
			jsonData = System.Text.Encoding.UTF8.GetString (www.bytes, 0, www.bytes.Length - 0);

			JSONObject json = new JSONObject (jsonData);
			Debug.Log (json);
			string imgName = json [0] [0] [3].ToString ().Replace ('"', ' ').Trim ();
			string imageUrl = "http://whitethoughtsdigital.com/dev/armall/targets/business/" + imgName;
			Debug.Log ("URL........." + "http://whitethoughtsdigital.com/dev/armall/targets/business/" + imgName);
			Texture2D texture;
			texture = new Texture2D(1,1);
			using(WWW www1 = new WWW(imageUrl))
			{
				
				yield return www1;
				www1.LoadImageIntoTexture (texture);
				//GetComponent<Renderer> ().material.mainTexture = texture;
				storeImageName.GetComponent<RawImage>().texture = texture;
			
			}

			Debug.Log("storename......."+json[0][0][2].ToString());
			storeName.GetComponent<Text> ().text = json [0] [0] [2].ToString ().Replace ('"', ' ').Trim ();
			Debug.Log ("city.........."+json[0][0][4].ToString());
			city.GetComponent<Text>().text = json [0] [0] [4].ToString ().Replace ('"', ' ').Trim ();
			Debug.Log ("address......."+json[0][0][10].ToString());
			address.GetComponent<Text>().text = json [0] [0] [10].ToString ().Replace ('"', ' ').Trim ();
			Debug.Log ("pincode........"+json[0][0][16].ToString());
			pinCode.GetComponent<Text>().text = json [0] [0] [16].ToString ().Replace ('"', ' ').Trim ();
			Debug.Log ("email.........."+json[0][0][14].ToString());
			email.GetComponent<Text>().text = json [0] [0] [14].ToString ().Replace ('"', ' ').Trim ();
			Debug.Log ("phno..........."+json[0][0][15].ToString());
			phno.GetComponent<Text>().text = json [0] [0] [15].ToString ().Replace ('"', ' ').Trim ();
			//Debug.Log ("json count....."+json[0].Count);


			//mallImg.Add (json[0][i][3].ToString().Replace('"', ' ').Trim());
			



		} 

//		foreach (KeyValuePair<string, string> MNI in mallNameImg) 
//		{
//			Debug.Log ("MallName........" + MNI.Key);
//			Debug.Log ("MallImage........." + MNI.Value);
//			//string sceneNameShort = Path.GetFileNameWithoutExtension(sceneName);
//			//lableText.text = sceneNameShort;
//			string imageUrl = "http://whitethoughtsdigital.com/dev/armall/targets/business/" + MNI.Value;
//			Debug.Log ("URL........." + "http://whitethoughtsdigital.com/dev/armall/targets/business/" + MNI.Value);
//			Texture2D texture;
//			texture = new Texture2D(1,1);
//			using(WWW www1 = new WWW(imageUrl))
//			{
//				yield return www1;
//				www1.LoadImageIntoTexture (texture);
//				//GetComponent<Renderer> ().material.mainTexture = texture;
//				prefab.GetComponent<RawImage>().texture = texture;
//
//			}
//			text.GetComponent<Text> ().text = MNI.Key;
//			var clone = Instantiate (prefab.gameObject) as GameObject;
//			//clone.GetComponent<Button> ().AddEventListener1234 (MNI.Key, LoadAssetBundleScene);
//
//			clone.SetActive (true);
//			clone.transform.SetParent (rootContainer);
//
//		}
//
//		mallNameImg.Clear ();

	}

//	public void LoadAssetBundleScene(string sceneName)
//	{
//		SceneManager.LoadScene ("Stores");
//	}

	// Update is called once per frame
	void Update () {

	}

	public void OnMenu()
	{
		SceneManager.LoadScene ("MainVuforiaScene");

	}

	public void OnGame()
	{
		SceneManager.LoadScene ("Games");
	}
	public void Home()
	{
		SceneManager.LoadScene ("MainUIScene");
		GM.Instance.isTrue = true;
	}
	public void wallet()
	{
		SceneManager.LoadScene ("wallet");
	}
	public void hotDeals()
	{
		SceneManager.LoadScene ("HotDeals");
	}

}
