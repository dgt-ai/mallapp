﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class StreamVideo : MonoBehaviour {

	public RawImage rawImage;
	public VideoPlayer videoPlayer;
	public AudioSource audioSource;
	public GameObject Menu;
	// Use this for initialization
	void Start () {
		StartCoroutine (PlayVideo());
	}

	IEnumerator PlayVideo()
	{
		videoPlayer.Prepare ();
		WaitForSeconds waitForSeconds = new WaitForSeconds (1);
		while(!videoPlayer.isPrepared)
		{

			yield return waitForSeconds;
			break;
		}

		rawImage.texture = videoPlayer.texture;
		videoPlayer.Play ();
		audioSource.Play ();
	}
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.P) && videoPlayer.isPlaying)
		{
			videoPlayer.Pause ();

		}
		else if(Input.GetKeyDown(KeyCode.P) && !videoPlayer.isPlaying)
		{
			videoPlayer.Play ();
		}


	}

	public void HomeButton()
	{
		SceneManager.LoadScene ("MainUIScene");
		GM.Instance.isTrue = true;
	}

	public void OnGame()
	{
		SceneManager.LoadScene ("Games");
	}
	public void Home()
	{
		SceneManager.LoadScene ("MainUIScene");
		GM.Instance.isTrue = true;
	}
	public void wallet()
	{
		SceneManager.LoadScene ("wallet");
	}
	public void hotDeals()
	{
		SceneManager.LoadScene ("HotDeals");
	}

	public void MenuButton()
	{
		Menu.SetActive (true);
		Menu.gameObject.GetComponent<RectTransform> ().DOLocalMove (new Vector3 (-272f, 58.2f, 0), 0.1f, false);
	}

	public void MenuBackButton()
	{
		Menu.gameObject.GetComponent<RectTransform> ().DOLocalMove (new Vector3 (-878.2f, 58.2f, 0), 0.1f, false);
	}

	public void VideoButton()
	{
		SceneManager.LoadScene ("360Video");
	}
}
