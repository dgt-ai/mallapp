﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using NatShareU;


public class ScreenShot : MonoBehaviour {


	public Image shareImage;

	// Use this for initialization
	void Start () 
	{

		StartCoroutine (LoadScreenshot());

	}

	IEnumerator LoadScreenshot()
	{
		var texture = new Texture2D(512, 512);

		texture.LoadImage(File.ReadAllBytes(ScanQRController.Instance.gameScreenShotPath));

		shareImage.sprite = Sprite.Create(texture, new Rect(0,0, texture.width, texture.height), new Vector2(.5f,.5f), 100);

		yield return null;

	}

	public void OnCancel()
	{
		GameObject ar = GameObject.Find ("ScanQRController");
		var arScript = ar.GetComponent<ScanQRController> ();
		arScript.ActiveButtons ();
		Destroy (gameObject);
	}

	public void ShareButton()
	{
		NatShare.ShareImage (ScanQRController.Instance.imageTemp);

		//ScanQRController.Instance.imageTemp.Share ();
	}

//	public void SaveToCameraRoll()
//	{
//		NatShare.SaveToCameraRoll (ScanQRController.Instance.imageTemp);
//	}
}
