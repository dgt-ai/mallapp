﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class GM : MonoBehaviour {


	private static GM instance = null;
	public static GM Instance
	{
		get
		{
			if (instance == null)
			{
				instance = GameObject.Find ("GM").GetComponent<GM> ();
			}

			return instance;
		}
	}

	[HideInInspector] public bool isTrue = false;

	void Awake ()
	{


		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
		else
		{
			Destroy (gameObject);
		}


	}

}
