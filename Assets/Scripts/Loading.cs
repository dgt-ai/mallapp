﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loading : MonoBehaviour {


	public GameObject[] loadImage;

	[HideInInspector] public int n;



	void Start()
	{
		StartCoroutine (loading());
	}

	IEnumerator loading()
	{



		yield return new WaitForSeconds (0.1f);

		loadImage [0].SetActive (true);

		n++;

		yield return new WaitForSeconds (0.1f);

		loadImage [1].SetActive (true);

		n++;

		yield return new WaitForSeconds (0.1f);

		loadImage [2].SetActive (true);

		n++;

		yield return new WaitForSeconds (0.1f);

		loadImage [3].SetActive (true);

		n++;

		yield return new WaitForSeconds (0.1f);


		Reloading ();

//		yield return new WaitForSeconds (2.0f);

//		splashPanel.SetActive(false);
//		categoriesPanel.SetActive(true);
//		stopLoading = false;


	}

	void Reloading()
	{

		if (n == 4) 
		{
			n = 0;

			for (int i = 0; i < 4; i++)
			{
				loadImage [i].SetActive (false);

			}

			StartCoroutine (loading());
		}
	}


}
