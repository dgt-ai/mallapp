﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;

public static class ButtonExtension1234
{
	public static void AddEventListener1234<T>(this Button button, T param, Action<T> OnClick )
	{
		button.onClick.AddListener (delegate 
			{
				OnClick(param);	

			}); 

	}
}

public class StoresLoading : MonoBehaviour {

	public string[] urls;

	[Header("UI Stuff")]
	public Transform rootContainer;
	public RawImage prefab;
	public Text text;

	static Dictionary<string, string> mallNameImg = new Dictionary<string, string> ();
	// Use this for initialization
	IEnumerator Start () 
	{

		string url = "http://www.whitethoughtsdigital.com/dev/armall/getBusinessList.php";
		WWWForm form1 = new WWWForm();
		form1.AddField ("bid",7);
		//		form1.AddField ("flag",1);
		WWW www = new WWW (url,form1);
		//string checkCouponmsg = www1.text;
		yield return www;
		string jsonData = "";
		if (www.error == null)
		{
			jsonData = System.Text.Encoding.UTF8.GetString (www.bytes, 0, www.bytes.Length - 0);

			JSONObject json = new JSONObject (jsonData);
			Debug.Log (json);

			Debug.Log(json.type);
			Debug.Log("json data......."+json[0][0][3].ToString());
			Debug.Log("json data......."+json[0][1][3].ToString());

			Debug.Log ("json count....."+json[0].Count);

			for (int i = 0; i < json [0].Count; i++) 
			{
				//mallImg.Add (json[0][i][3].ToString().Replace('"', ' ').Trim());
				mallNameImg.Add(json[0][i][2].ToString().Replace('"', ' ').Trim(), json[0][i][3].ToString().Replace('"', ' ').Trim());
			}



		} 

		foreach (KeyValuePair<string, string> MNI in mallNameImg) 
		{
			Debug.Log ("MallName........" + MNI.Key);
			Debug.Log ("MallImage........." + MNI.Value);
			//string sceneNameShort = Path.GetFileNameWithoutExtension(sceneName);
			//lableText.text = sceneNameShort;
			string imageUrl = "http://whitethoughtsdigital.com/dev/armall/targets/business/" + MNI.Value;
			Debug.Log ("URL........." + "http://whitethoughtsdigital.com/dev/armall/targets/business/" + MNI.Value);
			Texture2D texture;
			texture = new Texture2D(1,1);
			using(WWW www1 = new WWW(imageUrl))
			{
				yield return www1;
				www1.LoadImageIntoTexture (texture);
				//GetComponent<Renderer> ().material.mainTexture = texture;
				prefab.GetComponent<RawImage>().texture = texture;

			}
			text.GetComponent<Text> ().text = MNI.Key;
			var clone = Instantiate (prefab.gameObject) as GameObject;
			clone.GetComponent<Button> ().AddEventListener1234 (MNI.Key, LoadAssetBundleScene);

			clone.SetActive (true);
			clone.transform.SetParent (rootContainer);

		}

		mallNameImg.Clear ();

	}

	public void LoadAssetBundleScene(string sceneName)
	{
		SceneManager.LoadScene ("Stores");
	}

	// Update is called once per frame
	void Update () {

	}
}
