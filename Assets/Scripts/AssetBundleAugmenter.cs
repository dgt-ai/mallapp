﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using Vuforia;
public class AssetBundleAugmenter : MonoBehaviour, ITrackableEventHandler
{
    public string AssetName;
    public int Version;
    private GameObject mBundleInstance = null;
    private TrackableBehaviour mTrackableBehaviour;
    private bool mAttached = false;
	public GameObject internetConnectionBar;
	public GameObject internetConnectionBar1;
    void Start()
    {
		StartCoroutine(DownloadAndCache());
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}

    }

	void Update()
	{
		if (Application.internetReachability == NetworkReachability.NotReachable) {

			Debug.Log ("No internet connection");
			internetConnectionBar1.SetActive (true);

		} else {
			internetConnectionBar1.SetActive (false);
			internetConnectionBar.SetActive (false);
		
		}
	}


    // Update is called once per frame
    IEnumerator DownloadAndCache()
    {
		if(Application.internetReachability == NetworkReachability.NotReachable)
		{
			Debug.Log ("No internet connection");
			internetConnectionBar.SetActive (true);
		}
		else
		{
			CleanCache ();
			Debug.Log ("Internet connection available");
			internetConnectionBar.SetActive (false);
			while (!Caching.ready)
				yield return null;
			// example URL of file on PC filesystem (Windows)
			// string bundleURL = "file:///D:/Unity/AssetBundles/MyAssetBundle.unity3d";
			// example URL of file on Android device SD-card
			string bundleURL = "https://s3.amazonaws.com/assetbundleakhil/shootinggame";
			using (WWW www = WWW.LoadFromCacheOrDownload(bundleURL, Version))
			{
				yield return www;
				if (www.error != null)
					throw new UnityException("WWW Download had an error: " + www.error);
				AssetBundle bundle = www.assetBundle;
				if (AssetName == "")
				{
					mBundleInstance = Instantiate(bundle.mainAsset) as GameObject ;
					mBundleInstance.transform.SetParent (GameObject.Find("ImageTarget").transform,false);


				}
				else
				{
					mBundleInstance = Instantiate(bundle.LoadAsset(AssetName)) as GameObject;
					mBundleInstance.transform.SetParent (GameObject.Find("ImageTarget").transform,false);
				}
			}
		//}
        
		}
	}

	public static void CleanCache ()
	{
		if (Caching.ClearCache()) 
		{
			Debug.Log("Successfully cleaned the cache.");
		}
		else 
		{
			Debug.Log("Cache is being used.");
		}
	} 
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            if (!mAttached && mBundleInstance)
            {
                // if bundle has been loaded, let's attach it to this trackable
                mBundleInstance.transform.parent = this.transform;
                mBundleInstance.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                mBundleInstance.transform.localPosition = new Vector3(0.0f, 0.15f, 0.0f);
                mBundleInstance.transform.gameObject.SetActive(true);
                mAttached = true;
            }
        }
    }
}
