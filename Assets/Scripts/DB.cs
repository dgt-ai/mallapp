﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using MiniJSON;
using System;
using UnityEngine.SceneManagement;


public class DB : MonoBehaviour {

	[HideInInspector]	private static DB instance = null;

	//
	[HideInInspector] public int picValue;


	public static DB Instance 
	{
		get { 

			if (instance == null) {

				instance = GameObject.Find ("DB").GetComponent <DB> ();
			}

			return instance;
		}
	}



	void Awake ()
	{


		picValue = 0;

		//Debug.Log ("the path is>>>>" + Application.persistentDataPath);

		if (instance == null) 
		{
			instance = this;
			DontDestroyOnLoad (gameObject);
		} 
		else 
		{
			Destroy (gameObject);
		}

		DirectoryInfo dInfo = new DirectoryInfo (Application.persistentDataPath + "/LocalData/");
		if (!dInfo.Exists) {
			dInfo.Create ();
			DB.Instance.CreateUserDefaults ();
		} else {

			DB.Instance.GetUserData ();
		}



	}


	public void CreateUserDefaults ()
	{
		string aString = string.Empty;

		Dictionary <string, object> aDict = new Dictionary <string, object> ();


		aDict.Add ("picValue",picValue);

		string content = Json.Serialize (aDict);

		File.WriteAllText (Application.persistentDataPath + "/LocalData/" + "localdata.json", content);
	}



	public void SaveData ()
	{
		Dictionary <string, object> aDict = new Dictionary <string, object> ();


		aDict.Add ("picValue",picValue);



		string content = Json.Serialize (aDict);

		if (File.Exists (Application.persistentDataPath + "/LocalData/" + "localdata.json"))

			File.WriteAllText (Application.persistentDataPath + "/LocalData/" + "localdata.json", content);
	}


	public void GetUserData ()
	{
		if (File.Exists (Application.persistentDataPath + "/LocalData/" + "localdata.json")) 
		{
			StreamReader read = new StreamReader (Application.persistentDataPath + "/LocalData/" + "localdata.json");
			string data = read.ReadToEnd ();
			read.Close ();

			Dictionary<string, object> localdata = Json.Deserialize (data) as Dictionary<string, object>;

			picValue = Convert.ToInt32 (localdata["picValue"]);


		}
	}


	void OnApplicationQuit()
	{
		SaveData ();
	}
}
