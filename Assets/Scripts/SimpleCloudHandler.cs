﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SimpleCloudHandler : MonoBehaviour, ICloudRecoEventHandler {

	public static SimpleCloudHandler instance = null;
	private CloudRecoBehaviour mCloudRecoBehaviour;
	private bool mIsScanning = false;
	//private string mTargetMetadata = "";
	public ImageTargetBehaviour ImageTargetTemplate;
	public GameObject internetConnectionBar;
	public GameObject internetConnectionBar1;
	//

	public GameObject player;
	public string id;
	//

	public VideoClip videoToPlay;

	public VideoPlayer videoPlayer;
	public VideoSource videoSource;

	public AudioSource audioSource;


	public Text _text;

	public GameObject scanningButton;
	public GameObject exitButton;

    public string isType;
    public string gameUrl;

    public GameObject wireHair;
    public GameObject shootButton;
    void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
	}
	// Use this for initialization
	void Start () {

		if (Application.internetReachability == NetworkReachability.NotReachable) {
			Debug.Log ("No internet connection");
			internetConnectionBar.SetActive (true);
		}
		// register this event handler at the cloud reco behaviour
		else {

			internetConnectionBar.SetActive (false);
			mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour> ();

			if (mCloudRecoBehaviour) {
				mCloudRecoBehaviour.RegisterEventHandler (this);
			}
		}
		videoPlayer.loopPointReached += EndReached;
		Debug.Log("Next Scene Start");
	}

	public void Update()
	{
		//Debug.Log ("Calling update");
		if (Application.internetReachability == NetworkReachability.NotReachable) {
			internetConnectionBar1.SetActive (true);

		} else {
			internetConnectionBar1.SetActive (false);
			internetConnectionBar.SetActive (false);
		}
	}



	public void OnInitialized() {
		Debug.Log ("Cloud Reco initialized");
	}
	public void OnInitError(TargetFinder.InitState initError) {
		Debug.Log ("Cloud Reco init error " + initError.ToString());
	}
	public void OnUpdateError(TargetFinder.UpdateState updateError) {
		Debug.Log ("Cloud Reco update error " + updateError.ToString());
	}


	public void OnStateChanged(bool scanning) {
		mIsScanning = scanning;
		if (scanning)
		{
			// clear all known trackables
			var tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
			tracker.TargetFinder.ClearTrackables(false);
		}
	}


	// Here we handle a cloud target recognition event
	public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult) {

		// Build augmentation based on target
		if (ImageTargetTemplate) {
			// enable the new result with the same ImageTargetBehaviour:
			ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
			ImageTargetBehaviour imageTargetBehaviour =
				(ImageTargetBehaviour)tracker.TargetFinder.EnableTracking(
					targetSearchResult, ImageTargetTemplate.gameObject);
		}


//		// stop the target finder (i.e. stop scanning the cloud)
		mCloudRecoBehaviour.CloudRecoEnabled = false;

		string url = "http://www.whitethoughtsdigital.com/dev/armall/getAssetBundleTarget.php";

		 id = targetSearchResult.UniqueTargetId;

		_text.text = "Image Detected...";


		Debug.Log ("tbl_target_id,,,,,,,,,,"+id);

		WWWForm form = new WWWForm();

		form.AddField ("targetID",id);

		WWW www = new WWW (url, form);

		StartCoroutine (WaitForRequest(www));

	}


    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        string jsonData = "";
        // check for errors
        if (www.error == null)
        {
            jsonData = System.Text.Encoding.UTF8.GetString(www.bytes, 0, www.bytes.Length - 0);
            JSONObject json = new JSONObject(jsonData);
			Debug.Log ("jsosn file-------"+json);
            gameUrl = json[0][0][0].ToString();
            isType = json[0][0][1].ToString();
            string isTypeTrim = isType.Replace('"', ' ').Trim();
            Debug.Log(gameUrl);
            Debug.Log(isTypeTrim);

            if (isTypeTrim == "0")
            {
                Debug.Log("entered if part");
                GameObject.Find("ImageTarget").GetComponent<AssetBundleAugmenter>().enabled = false;
                wireHair.SetActive(false);
                shootButton.SetActive(false);

                Debug.Log("enter,,,,");
                string imageName = gameUrl.Replace('"', ' ').Trim();

                Debug.Log(".............." + imageName);

                string _url = "https://d33qpa0ixpc550.cloudfront.net/" + imageName;

                _text.text = "Downloading Video..";

                Debug.Log("URL= " + _url);

                // Vide clip from Url
                videoPlayer.source = VideoSource.Url;

                videoPlayer.url = _url;

                //Set Audio Output to AudioSource
                videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

                //Assign the Audio from Video to AudioSource to be played
                videoPlayer.EnableAudioTrack(0, true);
                videoPlayer.SetTargetAudioSource(0, audioSource);

                //Set video To Play then prepare Audio to prevent Buffering
                //			videoPlayer.clip = videoToPlay;
                videoPlayer.Prepare();

                //			videoPlayer.prepareCompleted+ =

                _text.text = "Preparing Video..";

                //Wait until video is prepared
                while (!videoPlayer.isPrepared)
                {
                    yield return null;
                }


                Debug.Log("Done Preparing Video");

                videoPlayer.Play();

                audioSource.Play();

                Debug.Log("Playing Video");

                _text.text = "Playing Video..";




            }
            else
            {
                Debug.Log("Entered else part");
                GameObject.Find("ImageTarget").GetComponent<AssetBundleAugmenter>().enabled = true;
                wireHair.SetActive(true);
                shootButton.SetActive(true);

            }



        }


    }
	void EndReached(UnityEngine.Video.VideoPlayer vp)
	{
		//vp.playbackSpeed = vp.playbackSpeed / 10.0F;
		Debug.Log("Next Scene");
		SceneManager.LoadScene ("scanQR");
	}


    void OnGUI() {
//		// Display current 'scanning' status
//		GUI.Box (new Rect(100,100,200,50), mIsScanning ? "Scanning"+mIsScanning : "Not scanning"+mIsScanning);
//		// Display metadata of latest detected cloud-target
//		GUI.Box (new Rect(100,200,200,50), "Metadata: " + mTargetMetadata);
		// If not scanning, show button
		// so that user can restart cloud scanning


		if (mIsScanning)
		{
			_text.text = "Scanning...";
		}
//		else
//		{
//			_text.text = "NOT SCANNING..";
//
//		}


		if (!mIsScanning) {
			scanningButton.SetActive (true);
			exitButton.SetActive (true);

		} 
		else
		{
			scanningButton.SetActive (false);
			exitButton.SetActive (false);

		}
	}


	public void OnRestart()
	{
        GameObject.Find("ImageTarget").GetComponent<AssetBundleAugmenter>().enabled = false;
        wireHair.SetActive(false);
        shootButton.SetActive(false);
        OnTrackingLost ();
		videoPlayer.Stop ();
		mCloudRecoBehaviour.CloudRecoEnabled = true;	

		scanningButton.SetActive (false);
		exitButton.SetActive (false);
		CleanCache ();
		TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
		TrackerManager.Instance.GetTracker<ObjectTracker> ().Start ();
	}

	public static void CleanCache ()
	{
		if (Caching.ClearCache()) 
		{
			Debug.Log("Successfully cleaned the cache.");
		}
		else 
		{
			Debug.Log("Cache is being used.");
		}
	} 

	public void OnTrackingFound()
	{
//		GameObject.Find ("bottle1").GetComponent<Rigidbody> ().isKinematic = false;
//		GameObject.Find ("bottle2").GetComponent<Rigidbody> ().isKinematic = false;
//		GameObject.Find ("bottle3").GetComponent<Rigidbody> ().isKinematic = false;
//		GameObject.Find ("bottle4").GetComponent<Rigidbody> ().isKinematic = false;
//		GameObject.Find ("bottle5").GetComponent<Rigidbody> ().isKinematic = false;
	}
	public void OnTrackingLost()
	{
		player.GetComponent<Renderer> ().enabled = false;

//		GameObject.Find ("bottle1").GetComponent<Rigidbody> ().isKinematic = true;
//		GameObject.Find ("bottle2").GetComponent<Rigidbody> ().isKinematic = true;
//		GameObject.Find ("bottle3").GetComponent<Rigidbody> ().isKinematic = true;
//		GameObject.Find ("bottle4").GetComponent<Rigidbody> ().isKinematic = true;
//		GameObject.Find ("bottle5").GetComponent<Rigidbody> ().isKinematic = true;

	}

	public void HomeButton()
	{
		SceneManager.LoadScene ("MainUIScene");
	}
	public void OnQuit()
    {
		Application.Quit();
	}

}
