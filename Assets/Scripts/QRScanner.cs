﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Vuforia;
using System.IO;
using ZXing;
using ZXing.QrCode;
using UnityEngine.UI;    
using NatShareU;

public class QRScanner : MonoBehaviour {
	private WebCamTexture camTexture;
	private Rect screenRect;

	public Image qrImage;
	public RawImage imageQr;
	public RawImage dImage;
	string dealName;
	string dealDesc;
	string dealImg;
	string dealCode;
	public Text couponMessage; 
	public GameObject dealNameText;
	public GameObject dealNameDesc;
	public GameObject couponPanel;
	public GameObject couponMessageholder;
	//public GameObject gameData;
	//public GameObject ddata;
	[HideInInspector]public GameObject loadPrefab;

	public static QRScanner instance = null;
	bool flage = true;

	void Awake()//RK
	{
		
		 
		if (instance == null) 
			instance = this;
		 else if (instance != this) 

			Destroy (gameObject);
		


	}

	private static Color32[] Encode(string textForEncoding, int width, int height) {
		var writer = new BarcodeWriter {
			Format = BarcodeFormat.QR_CODE,
			Options = new QrCodeEncodingOptions {
				Height = height,
				Width = width
			}
		};
		return writer.Write(textForEncoding);
	}

	public Texture2D generateQR(string text) {
		var encoded = new Texture2D (256, 256);
		var color32 = Encode(text, encoded.width, encoded.height);
		encoded.SetPixels32(color32);
		encoded.Apply();
		return encoded;
	}
	//public string[] getDealData(string arr){
	public void getDealData(string arr){
		string url = "http://www.whitethoughtsdigital.com/dev/armall/getCampgDeal.php";
		WWWForm form = new WWWForm();
		form.AddField ("targetID",arr);
		WWW www = new WWW (url, form);
		StartCoroutine (WaitForRequest(www));

	}

	IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
		string jsonData = "";
		// check for errors
		if (www.error == null)
		{
			jsonData = System.Text.Encoding.UTF8.GetString (www.bytes, 0, www.bytes.Length - 0);

			JSONObject json = new JSONObject (jsonData);
			dealName = json[0][0][0].ToString();
			dealDesc = json[0][0][1].ToString();
			dealImg = json[0][0][2].ToString();
			dealCode = json[0][0][3].ToString();

		} 
		  
	} 



	IEnumerator Startx()
	{
		string newDealImg = dealImg.Replace('"', ' ').Trim();
		string URL = "http://www.whitethoughtsdigital.com/dev/armall/targets/"+newDealImg;
		Texture2D texture;
		texture = new Texture2D(1,1);
		using(WWW www1 = new WWW(URL))
		{
			yield return www1;
			www1.LoadImageIntoTexture (texture);
			//GetComponent<Renderer> ().material.mainTexture = texture;
			dImage.GetComponent<RawImage>().texture = texture;

		}

	}


	void OnGUI()
	{
		
		string tid = SimpleCloudHandler.instance.id;
		if (flage == true) {

		getDealData (tid);
		string newDealCode = dealCode.Replace('"', ' ').Trim();
		string imei = SystemInfo.deviceUniqueIdentifier;
		newDealCode = newDealCode + "D" + imei;
			StartCoroutine (checkCoupon (newDealCode));
			flage = false;
			Debug.Log ("once enter"+flage);
		} 
		//call api to check -- newDealCode
		Debug.Log ("outside" + flage);

	}

	public void updateDealData(string did){
		string url = "http://www.whitethoughtsdigital.com/dev/armall/updateDealRedeem.php";
		WWWForm form1 = new WWWForm();
		form1.AddField ("dealID",did);
		form1.AddField ("flag",0);
		WWW www1 = new WWW (url, form1);
		string checkCouponmsg = www1.text;
		Debug.Log ("Field Updated" + checkCouponmsg );


	}


	IEnumerator checkCoupon(string newDealCode){
		Debug.Log ("entered" + newDealCode); 
		string url = "http://www.whitethoughtsdigital.com/dev/armall/checkCouponData.php";
		WWWForm form1 = new WWWForm();
		form1.AddField ("dealCode", newDealCode);
		WWW www2 = new WWW (url, form1);
		yield return www2;
		string checkCoupon = www2.text;

		if (checkCoupon == "Exists") {
			loadPrefab = Instantiate (Resources.Load("Prefabs/couponsPrefab")) as GameObject;
			loadPrefab.transform.SetParent (GameObject.Find("Canvas").transform, false);
//			couponMessage.text = "Coupon Exits";
//			couponMessageholder.SetActive (true);
			couponPanel.SetActive (false);


			Debug.Log (" Coupon Exits");
			flage = false;
		} 
		 else if (checkCoupon == "Expired") {
			couponPanel.SetActive (false);
			couponMessageholder.SetActive (true);
			couponMessage.text = "Deal Expired";
			Debug.Log (" Coupon Exits");
			flage = false;
		}
		else {
			Debug.Log ("coupan genertated" + newDealCode);
			couponMessageholder.SetActive (false);
			couponPanel.SetActive (true);
			Texture2D myQR = generateQR(newDealCode);
			//var imgURL = "http://whitethoughtsdigital.com/dev/armall/targets/new_card2.jpg";
			imageQr.texture = myQR;
			string newDealName = dealName.Replace('"', ' ').Trim();
			string newDealDesc = dealDesc.Replace('"', ' ').Trim();
			//StartCoroutine (LoadImage());
			dealNameText.GetComponent<Text> ().text = newDealName;
			dealNameDesc.GetComponent<Text> ().text = newDealDesc;
			StartCoroutine (Startx());
			updateDealData (newDealCode);

			ScanQRController.instance.OnTake ();
			Debug.Log ("Updated function called"+newDealCode );

			flage = false;
		}

	}
}
