﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;
using System;

public class DownloadAssetBundle : MonoBehaviour {
	[HideInInspector]public string url;
	// Use this for initialization
	void Start () {
		//https://s3.amazonaws.com/assetbundleakhil/model.dinningtable
		//C:\Users\DGT-AI\Documents\AssertBundleDemo\Assets\AssetBundles\model.dinningtable
		url = "https://s3.amazonaws.com/assetbundleakhil/myasset";
		//WWW www = new WWW (url);
		StartCoroutine (DownloadAsset());
	}

	IEnumerator DownloadAsset()
	{
		WWW www = new WWW (url);
		yield return www;
		AssetBundle assetbundle = www.assetBundle;
		if (www.error == null) {
			GameObject assetDemo = (GameObject)assetbundle.LoadAsset ("DinningTable");
			Instantiate (assetDemo,Vector3.zero,Quaternion.identity);
		} else {
			Debug.Log (www.error);
		}
	}


}
