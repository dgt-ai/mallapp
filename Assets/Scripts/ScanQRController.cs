﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using NatShareU;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class ScanQRController : MonoBehaviour {
	
	public GameObject screenShotButton;
	[HideInInspector]public string gameScreenShotPath;
	[HideInInspector] public Texture2D imageTemp;

	[HideInInspector]public static ScanQRController instance = null;
	public GameObject Menu;
	public static ScanQRController Instance 
	{
		get
		{
			if (instance == null)  
			{
				instance = GameObject.Find ("ScanQRController").GetComponent<ScanQRController> ();
			}

			return instance;
		}
	}

	void Awake()
	{
		if(instance == null)
		{
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
		else
		{
			Destroy (gameObject);
		}
	}

	public void OnTake()
	{
		screenShotButton.SetActive (false);

		DB.Instance.GetUserData ();
		gameScreenShotPath = Application.persistentDataPath + "/Game"+DB.Instance.picValue+".png";
		if(File.Exists(gameScreenShotPath)) File.Delete(gameScreenShotPath);
		Texture2D screenShot = new Texture2D(Screen.width, Screen.height);
		screenShot.ReadPixels(new Rect(0,0,Screen.width,Screen.height),0,0);
		screenShot.Apply();
		byte[] bytes = screenShot.EncodeToPNG();
		//		Object.Destroy(screenShot);
		File.WriteAllBytes(gameScreenShotPath, bytes);
		imageTemp = screenShot;
		DB.Instance.picValue++;
		DB.Instance.SaveData ();

		Invoke ("OnLoadPrefab",10.0f);
	}

	public void OnLoadPrefab()
	{
//		GameObject load = Instantiate (Resources.Load("Prefabs/ScreenShotPanel")) as GameObject;
//		load.transform.SetParent (GameObject.Find ("Canvas").transform, false);

		NatShare.SaveToCameraRoll (imageTemp);
		Debug.Log ("Screenshot saved to camera roll");

	}

	public void ActiveButtons()
	{
		screenShotButton.SetActive (true);
	}

	public void MenuButton()
	{
		Menu.SetActive (true);
		Menu.gameObject.GetComponent<RectTransform> ().DOLocalMove (new Vector3 (-272f, 58.2f, 0), 0.1f, false);
	}

	public void MenuBackButton()
	{
		Menu.gameObject.GetComponent<RectTransform> ().DOLocalMove (new Vector3 (-878.2f, 58.2f, 0), 0.1f, false);
	}
}
