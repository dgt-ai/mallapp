﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class MainUI : MonoBehaviour {

	public GameObject splashPanel;
	public GameObject mainPanel;
	public GameObject nearbyPanel;
	public GameObject Menu;

	void Start()
	{
		splashPanel.SetActive (true);
		StartCoroutine (Load());

		if (GM.Instance.isTrue)
		{
			GM.Instance.isTrue = false;
			mainPanel.SetActive (true);

		}
		else
		{
			splashPanel.SetActive (true);    
			StartCoroutine (Load ());
		}
	}

	IEnumerator Load()
	{
		yield return new WaitForSeconds (2.0f);
		splashPanel.SetActive (false);
		mainPanel.SetActive (true);
	}

	public void OnMenu()
	{
		SceneManager.LoadScene ("MainVuforiaScene");

	}

	public void OnGame()
	{
		SceneManager.LoadScene ("Games");
	}
	public void Home()
	{
		SceneManager.LoadScene ("MainUIScene");
		GM.Instance.isTrue = true;
	}
	public void wallet()
	{
		SceneManager.LoadScene ("wallet");
	}
	public void hotDeals()
	{
		SceneManager.LoadScene ("HotDeals");
	}
	public void nearBuy()
	{
		mainPanel.SetActive (false);
		nearbyPanel.SetActive (true);
	}
	//
//	public void OnMenu1()
//	{
//		SceneManager.LoadScene ("AR_video_scn1");
//	}

	public void MenuButton()
	{
		Menu.SetActive (true);
		Menu.gameObject.GetComponent<RectTransform> ().DOLocalMove (new Vector3 (-272.0f, 58.2f, 0), 0.1f, false);
	}

	public void MenuBackButton()
	{
		Menu.gameObject.GetComponent<RectTransform> ().DOLocalMove (new Vector3 (-878.2f, 58.2f, 0), 0.1f, false);
	}
}
