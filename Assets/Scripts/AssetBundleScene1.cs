﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using DG.Tweening;

public static class ButtonExtension
{
	public static void AddEventListener<T>(this Button button, T param, Action<T> OnClick )
	{
		button.onClick.AddListener (delegate 
		{
				OnClick(param);	
			
		}); 
		
	}
}

public class AssetBundleScene1 : MonoBehaviour {

	public string[] urls;

	[Header("UI Stuff")]
	public Transform rootContainer;
	public Button prefab;
	public Text lableText;
	public GameObject Menu;

	static List<AssetBundle> assetBundle = new List<AssetBundle>();
	static List<string> scenePaths = new List<string> ();

	// Use this for initialization
	IEnumerator Start () 
	{
		
		if (assetBundle.Count == 0) 
		{
			int i = 0;
			while(i < urls.Length)
			{
				using (WWW www = new WWW (urls [i])) 
				{
					yield return www;
					if (!string.IsNullOrEmpty (www.error)) 
					{
						Debug.LogError (www.error);
						yield break;
					}
					assetBundle.Add (www.assetBundle);
					scenePaths.AddRange (www.assetBundle.GetAllScenePaths ());
				}
				i++;
			}
		}


		foreach (string sceneName in scenePaths) 
		{
			Debug.Log (Path.GetFileNameWithoutExtension(sceneName));
			string sceneNameShort = Path.GetFileNameWithoutExtension(sceneName);
			lableText.text = sceneNameShort;

			var clone = Instantiate (prefab.gameObject) as GameObject;

			clone.GetComponent<Button> ().AddEventListener (sceneNameShort, LoadAssetBundleScene);

			clone.SetActive (true);
			clone.transform.SetParent (rootContainer);
		}

	}

	public void LoadAssetBundleScene(string sceneName)
	{
		SceneManager.LoadScene (sceneName);
	}


	public void OnMenu()
	{
		SceneManager.LoadScene ("MainVuforiaScene");
	}

	public void OnGame()
	{
		SceneManager.LoadScene ("Games");
	}
	public void Home()
	{
		SceneManager.LoadScene ("MainUIScene");
		GM.Instance.isTrue = true;
	}
	public void wallet()
	{
		SceneManager.LoadScene ("wallet");
	}
	public void hotDeals()
	{
		SceneManager.LoadScene ("HotDeals");
	}

	public void ShootingGame()
	{
		SceneManager.LoadScene ("ShootingGame");
	}
	// Update is called once per frame
	void Update () {
		
	}


	public void MenuButton()
	{
		Menu.SetActive (true);
		Menu.gameObject.GetComponent<RectTransform> ().DOLocalMove (new Vector3 (-272.0f, 58.2f, 0), 0.1f, false);
	}

	public void MenuBackButton()
	{
		Menu.gameObject.GetComponent<RectTransform> ().DOLocalMove (new Vector3 (-878.2f, 58.2f, 0), 0.1f, false);
	}
}
